Rock Laser wallpaper submission for Plasma 6.

Created with:
- Inkscape
- Gimp

License: CC-BY-SA-4.0

The main wallpaper is a dark one:

![image](./wallpapers/04_rock_laser_plasma6_1920x1080.png)

There is also a light variation:

![image](./wallpapers/05_rock_laser_plasma6_light_1920x1080.png)

Some screenshots in context:

![image](./screenshots/rock_laser_wallpaper_screenshot_01.png)

![image](./screenshots/rock_laser_wallpaper_screenshot_02.png)

![image](./screenshots/rock_laser_wallpaper_screenshot_03.png)

![image](./screenshots/rock_laser_wallpaper_screenshot_04.png)